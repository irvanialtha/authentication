'use strict'

module.exports = {
    service : (data, error) =>{
        data.body = {
            error   : true,
            service : environment.app.name,
            message : error.message
        };
        if (environment.app.name != data.source) {
            amqp.publish(data.source,data.topic,data);
        }
    },
    server:(response, code, data) => {
        let message = `${code} - Unable to process request.`;
    
        if(data.body) {
            message = data.body.message ? data.body.message : data.message;
        }
    
        const rest = {
            error   : true,
            code    : code,
            service : data.body.service || '',
            message : message
        };
    
        response.status(code).json(rest);
    }
}
         
