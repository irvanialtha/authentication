
'use strict';

const 
    fs  = require(`fs`),
    obj = {};

fs.readdirSync(`${__dirname}/helper`).forEach((file) => {
    const src = require(`${__dirname}/helper/${file}`);

    obj[file.replace(/.js|.ts/g, ``)] = src;
});

 module.exports = { ... obj };