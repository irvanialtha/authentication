

`use strict`;
const
    fs    = require(`fs`);

const main = [

    // Error Handling
    new Promise((resolve, reject) => {

        process.on('SIGTERM', () => {
            process.emit(`cleanup`, `${environment.app.name} PID: ${process.pid} terminated\n`);
        });

        process.on('SIGHUP', () => {
            process.emit(`cleanup`, `${environment.app.name} PID: ${process.pid} hanged up\n`);
        });

        process.on('SIGINT', () => {
            process.emit(`cleanup`, `${environment.app.name} PID: ${process.pid} has been shutdown manually\n`);
        });

        process.on('SIGUSR1', () => {
            process.emit(`cleanup`, `${environment.app.name} PID: ${process.pid} is restarted\n`);
        });

        process.on('SIGUSR2', () => {
            process.emit(`cleanup`, `${environment.app.name} PID: ${process.pid} is restarted\n`);
        });

        process.on('terminate', () => {
            process.emit(`cleanup`, `${environment.app.name} with PID: ${process.pid} has been shutdown by system\n`);
        });

        process.on('cleanup', (message) => {
            if(environment.node.env === `production` ) {
                system.write(`[${date.now()}] ${message}`);
            }

            setTimeout(() => {
                process.exit();
            }, 50);
        });

        process.on('error-log', (message) => {
            console.error(message);
            stream.write(`[${date.now()}] ${message}\n`);

            setTimeout(() => {
                process.exit(1);
            }, 500);
        });

        process.on('system-log', (message) => {
            if(environment.node.env === `production`) console.log(message);
            system.write(`[${date.now()}] ${message}\n`)
        });

        if(!fs.existsSync(`${basedir}/logs`)) {
            fs.mkdirSync(`${basedir}/logs`);
        }

        if(!fs.existsSync(`${basedir}/protected`)) {
            fs.mkdirSync(`${basedir}/protected`);
        }

        resolve();
    }),

    // Load Controller
    new Promise((resolve) => {
        global.control = {};

        fs.readdirSync(`${basedir}/src/controller`).forEach((file) => {
            const
                name = file.replace(/.js|.ts/g, ``).toLowerCase(),
                src  = require(`${basedir}/src/controller/${file}`);

            global.control[name] = src;
        });

        resolve();
    }),

    // Load Models
    new Promise((resolve) => {
        global.model = {};

        fs.readdirSync(`${basedir}/src/model`).forEach((file) => {
            const
                name = file.replace(/.js|.ts/g, ``).toLowerCase(),
                src  = require(`${basedir}/src/model/${file}`);

            global.model[name] = src;
        });

        resolve();
    }),

    // Load Publishers
    new Promise((resolve) => {
        global.subscriber = {};

        fs.readdirSync(`${basedir}/src/subscriber`).forEach((file) => {
            const
                name = file.replace(/.js|.ts/g, ``).toLowerCase(),
                src  = require(`${basedir}/src/subscriber/${file}`);

            global.subscriber[name] = src;
        });

        resolve();
    }),

    // Load HttpRequest
    new Promise((resolve) => {
        global.http_request = {};
        resolve();
    }),

    // Load Services
    new Promise((resolve) => {
        global.service = require(`${basedir}/config/services.json`);;
        resolve();
    })

];

module.exports = () => {
    return new Promise((resolve, reject) => {
        Promise.all(main).then((value) => {
            resolve();
        }).catch((error) => {
            reject(error);
        });
    }).catch(async (error) => {
        
        console.error(error);
        setTimeout(() => {
            process.exit(1);
        }, 500);
    });
};
