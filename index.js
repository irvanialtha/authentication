`use strict`;

const 
    { service, amqp, mqtt, subscriber, database, redis} = require(`./app_module`);

let start = () => {
    return new Promise(async (resolve, reject) => {
        resolve({
            service : await service(),
            amqp    : await amqp(),
            db      : await database(),
            redis   : await redis()
        })
     }).then(async (data) => {
        subscriber();
     });
};

start()