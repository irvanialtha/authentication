`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`users`, {
        id        : {
            type      : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true,
            autoIncrement: true,
        },
        created_at : {
            type      : Sequelize.DATE,
            allowNull : true
        },
        status_id         : {
            type      : Sequelize.INTEGER,
            allowNull : false
        }
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'user'
    });

};