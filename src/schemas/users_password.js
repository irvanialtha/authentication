`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`users_password`, {
        id        : {
            type      : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true,
            autoIncrement: true,
        },
        user_id         : {
            type      : Sequelize.INTEGER,
            allowNull : false,
            references : {
                model : `users`,
                key   : `id`
            }
        },
        password         : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        created_at : {
            type      : Sequelize.DATE,
            allowNull : true
        },
        status_id         : {
            type      : Sequelize.STRING,
            allowNull : false
        },
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'user'
    });
};