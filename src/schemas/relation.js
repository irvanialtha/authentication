`use strict`;

module.exports = () => {
    db.users.hasMany(db.users_information, {
        foreignKey: 'user_id'
    });

    db.users_information.belongsTo(db.users, {
        foreignKey : `user_id`
    });

    db.users.hasMany(db.users_password, {
        foreignKey: 'user_id'
    });

    db.users_password.belongsTo(db.users, {
        foreignKey : `user_id`
    });


}