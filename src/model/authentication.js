`use strict`;

module.exports = {
    getuserinformation: (userid)=>{
        let output = {
                        error : true,
                        data : {}
                    };
        return new Promise((resolve,reject) =>{
            db.users.findOne({
                attributes : ['id'],
                include: [
                    {
                        model: db.users_information,
                        attributes : ['id','user_id','name','phone','address','city','zip','email'],
                        required: true,
                        where: {
                            status_id : {
                                [Op.eq] : 1,
                            }
                        }
                    },
                ],
                where:{
                    id :{
                        [Op.eq] : userid
                    }
                }
                
            }).then((rows) =>{
                if (rows) {
                    output = {
                        success : true,
                        data    : rows.dataValues.users_informations[0].dataValues
                    } 
                } 
                resolve(output)
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },
    getuserpassword: (email)=>{
        let output = {
                        error : true,
                        data : {}
                    };
        return new Promise((resolve,reject) =>{
            db.users.findOne({
                attributes : ['id'],
                include: [
                    {
                        model: db.users_information,
                        attributes : ['name','phone','address','city','zip','email'],
                        required: true,
                        where: {
                            status_id : {
                                [Op.eq] : 1,
                            },
                            email :{
                                [Op.eq] : email
                            }
                        }
                    },
                    {
                        model: db.users_password,
                        attributes : ['password'],
                        required: true,
                        where: {
                            status_id : {
                                [Op.eq] : 1,
                            }
                        }
                    }
                ]
                
            }).then((rows) =>{
                if (rows) {
                    output = {
                        success : true,
                        data : {
                                userid   : rows.dataValues.id,
                                password : rows.dataValues.users_passwords[0].password
                            }
                    } 
                } 
                resolve(output)
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    }
}