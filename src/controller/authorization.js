const authentication = require("../subscriber/authentication");

`use strict`;
const
    { redis }  = require(`${basedir}/app_module/src/helper`);
module.exports = {
    
    authorize : async(data) => {
        let token       = data.headers.token,
            userinfo    = await redis.get(`authentication::${token}`);
        if (!userinfo) {
            data.body.valid = false;
            amqp.publish(data.source,'auth.authorization',data);
            return;
       
        }
        data.body = {
            valid : true,
            env   : userinfo
        };
        amqp.publish(data.source,'auth.authorization',data);

    }
};