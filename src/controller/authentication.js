`use strict`;
const
    crypto           = require(`crypto-js`),
    bcrypt           = require('bcrypt'),
    { time, redis }  = require(`${basedir}/app_module/src/helper`);


const generate = (data) => {
    return crypto.AES.encrypt(JSON.stringify(data), environment.app.salt).toString();
}
module.exports = {
    
    authenticate : async (data) => {
        let email        = data.body.username,
            datapassword = await model.authentication.getuserpassword(email);

        if (datapassword.error) {
            data.body = {
                error : true,
                message : 'Unauthorized'
            };
            amqp.publish(data.source,'auth.output',data);
            return;
        }

        bcrypt.compare(data.body.password, datapassword.data.password, async (err, valid) => {

            if(!valid) {
                data.body = {
                    error : true,
                    message : 'Unauthorized'
                };
                amqp.publish(data.source,'auth.output',data);
                return;
            }

            let userinfo = await model.authentication.getuserinformation(datapassword.data.userid);
            
            if(userinfo.error) {
                data.body = {
                    error : true,
                    message : 'Failed get user information'
                };
                amqp.publish(data.source,'auth.output',data);
                return;
            }

            let result = {
                userinfo    : userinfo.data,
                time        : time.today(true)
            }

            let token = generate(result)

            redis.set(`authentication::${token}`,result, 2);

            data.body = {
                success : true,
                data : {
                    token
                }
            };
            amqp.publish(data.source,'auth.output',data);
        })

    },

    logout : async (data) => {
        let token = data.headers.token;
        redis.delete(`authentication::${token}`,result, 2);
        data.body = {
            data :{
                    message : 'logout'
                }
        };
        amqp.publish(data.source,'auth.output',data);
    }
};